﻿use master
go

drop database if exists [people_db_out]
go
create database [people_db_out]
go
use [people_db_out]
go



use [people_db_out]
go

drop table if exists [name_list]
go
create table [name_list](
[name]	nvarchar(20) not null,
[sex]	nchar(25) null
)
go

insert into [name_list]  ([name], [sex])
values 
(N'Буревій','m'),
(N'Даромира','w'),
(N'Водотрав','m'),
(N'Миробог','m');
go
